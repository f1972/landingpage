import React from 'react'

const PageNotFound = () => {
    return (
        <div className="font-semibold text-center mt-56">
            <h1>Error 404 Page Not Found!</h1>
        </div>
    )
}

export default PageNotFound;
